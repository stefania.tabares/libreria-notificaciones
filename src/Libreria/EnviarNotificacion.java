/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

/**
 *
 * @author USUARIO
 */
public class EnviarNotificacion implements Notificador {
	
	private String mensaje;

	public EnviarNotificacion(String mensaje) {
		
		this.mensaje = mensaje;

	}

	@Override
	public String enviar() {
		return mensaje;
	}
}
