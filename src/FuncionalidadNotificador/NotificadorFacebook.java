/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FuncionalidadNotificador;
import Libreria.Notificador;

/**
 *
 * @author USUARIO
 */
public class NotificadorFacebook  extends FuncionalidadNotificador {

	Notificador notificador;

	public NotificadorFacebook(Notificador notificador) {
		this.notificador = notificador;
	}

	@Override
	public String enviar() {

		return "\nSe ha enviado el mensaje por Facebook" + notificador.enviar();
	}
}
