/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FuncionalidadNotificador;
import Libreria.Notificador;

/**
 *
 * @author USUARIO
 */
public class NotificadorCorreoPersonal extends FuncionalidadNotificador {
	
	Notificador notificador;

	public NotificadorCorreoPersonal(Notificador notificador) {
		this.notificador = notificador;
	}

	@Override
	public String enviar() {
		return "\nSe ha enviado el mensaje por correo personal" + notificador.enviar();
	}   
}
