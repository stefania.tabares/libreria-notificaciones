/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import java.io.*;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;


import FuncionalidadNotificador.FuncionalidadNotificador;
import FuncionalidadNotificador.NotificadorCorreoEmpresarial;
import FuncionalidadNotificador.NotificadorCorreoPersonal;
import FuncionalidadNotificador.NotificadorFacebook;
import FuncionalidadNotificador.NotificadorSMS;
import Libreria.EnviarNotificacion;
import Libreria.Notificador;


/**
 *
 * @author USUARIO
 */
public class Aplicacion {
private static final String List = null;

	public static void main(String[] args) {

		String mensaje;

		Scanner teclado1 = new Scanner(System.in);
		
		//Se solicita al Cliente ingresar el mensaje

		System.out.println("Escriba el mensaje que desea enviar?");
		mensaje = teclado1.nextLine();
        
		//Se solicita ingresar un opción
                System.out.println("Lista de opciones para enviar el mensaje:");
                System.out.println("Opción 1: Facebook");
		System.out.println("Opción 2: Correo personal");
		System.out.println("Opción 3: Correo empresarial");
		System.out.println("Opción 4: SMS");
		System.out.println("Opción 5: Correo personal, correo empresarial y Facebook");
                System.out.println("Digite la opción por donde desea enviar el mensaje:");
        
		
		//Se realiza la validación del número ingresado por medio del condicional Switch 
		Scanner teclado = new Scanner(System.in);
		String opcion = teclado.next();
	    if (opcion.equals("1") || opcion.equals("2") || opcion.equals("3") || opcion.equals("4") || opcion.equals("5") )  {
			switch (opcion) {
			
			//Creamos los objetos de tipo notificador
			case "1":
				Notificador notificacion = new NotificadorFacebook(new EnviarNotificacion("\n" + mensaje));
				System.out.println(notificacion.enviar());
				break;

			case "2":
				Notificador notificacion1 = new NotificadorCorreoPersonal(new EnviarNotificacion("\n" + mensaje));
				System.out.println(notificacion1.enviar());
				break;
			case "3":
				Notificador notificacion2 = new NotificadorCorreoEmpresarial(new EnviarNotificacion("\n" + mensaje));
				System.out.println(notificacion2.enviar());
				break;

			case "4":
				Notificador notificacion3 = new NotificadorSMS(new EnviarNotificacion("\n" + mensaje));
				System.out.println(notificacion3.enviar());
				break;

			//Dando solución a la opción de usar varios tipos de notificación al mismo tiempo
                        // Se crea el caso donde la notifación será enviada por Correo personal y por el correo empresarial
			case "5":
				Notificador notificacion4 = new NotificadorCorreoPersonal(
						new NotificadorCorreoEmpresarial(new NotificadorFacebook(new EnviarNotificacion("\n" + mensaje))));
				System.out.println(notificacion4.enviar());
				break;

			}
		}else {
			System.out.println("Escoja una opción valida");
		}
		

	}
	

}
